<?php

namespace App\Http\Controllers;

use Request;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display Products listing
     */
    public function index()
    {
        $products = Product::all();

        return view('products.index', compact('products'));
    }

    /**
     * Creating a new Product
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Storing a new Product
     */
    public function store(Request $request)
    {
        $input = Request::all();
        Product::create($input);

        return redirect('products');
    }

    /**
     * Show one Product
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);

        return view('products.show', compact('product'));
    }

    /**
     * Editing a Product
    */
    public function edit($id)
    {
        $product = Product::findOrFail($id);

        return view('products.edit', compact('product'));
    }

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $input = Request::all();
        $product->update($input);

        return redirect('products');
    }

    /**
     * Removing a Product
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();

        return redirect('products.index');
    }
}
