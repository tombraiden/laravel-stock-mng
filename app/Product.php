<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * Protect from mass vulnerabilities
     */
    protected $fillable = ['name'];
}
