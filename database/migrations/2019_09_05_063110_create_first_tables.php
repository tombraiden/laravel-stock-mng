<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFirstTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('inputs', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->integer('total');
            $table->tinyInteger('api');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->text('name');
            $table->string('sku', 10)->unique();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('stocks', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->text('name');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('product_stocks', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('product_id')->unsigned();
            $table->bigInteger('stock_id')->unsigned();
            $table->integer('quantity');
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade')
                ->unsigned();
            $table->foreign('stock_id')
                ->references('id')
                ->on('stocks')
                ->onDelete('cascade')
                ->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('inputs_infos', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('input_id')->unsigned();
            $table->bigInteger('product_stock_id')->unsigned();
            $table->integer('amount');
            $table->foreign('input_id')
                ->references('id')
                ->on('inputs')
                ->onDelete('cascade')
                ->unsigned();
            $table->foreign('product_stock_id')
                ->references('id')
                ->on('product_stocks')
                ->onDelete('cascade')
                ->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->text('client');
            $table->text('client_info');
            $table->integer('total');
            $table->tinyInteger('api');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('orders_infos', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('order_id')->unsigned();
            $table->bigInteger('product_stock_id')->unsigned();
            $table->integer('amount');
            $table->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('cascade')
                ->unsigned();
            $table->foreign('product_stock_id')
                ->references('id')
                ->on('product_stocks')
                ->onDelete('cascade')
                ->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('products');
        Schema::dropIfExists('stocks');
        Schema::dropIfExists('product_stocks');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('orders_infos');
    }
}
