@extends('layouts.app')

@section('content')
    <h1>Products</h1>
    <div>
        @forelse ($products as $product)
            <h2>
                <a href="{{ url('/products', $product->id) }}">
                    {{ $product->name }}
                </a>
            </h2>
            <h3>
                {{ $product->sku }}
            </h3>
            <a href="{{ route('products.edit', $product->id) }}">
                @lang('Edit')
            </a>
            <div class="body">
                <pre>{{ $product }}</pre>
            </div>
        @empty
            <p>@lang('There are no products to display.')</p>
        @endforelse
    </div>
@stop