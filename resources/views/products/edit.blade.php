@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <br/>
            <h3 class="center">@lang('Edit Product')</h3>
            <br/>
            {!! Form::model($product, ['method' => 'PATCH', 'action' => 'ProductController@update', $product->id]) !!}
            @include('products.form', ['submitButtonText' => @lang('Edit')])
            {!! Form::close() !!}
        </div>
    </div>
@stop