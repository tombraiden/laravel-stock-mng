@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <br/>
            <h3 class="center">@lang('New Product')</h3>
            <br/>
            {!! Form::open(['action' => 'ProductController@store']) !!}
            @include('products.form', ['submitButtonText' => @lang('New')])
            {!! Form::close() !!}
        </div>
    </div>
@stop